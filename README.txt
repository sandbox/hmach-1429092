This module is intended to integrate sejmometr.pl api with drupal
It is triggered by cron job - if You do not run cron, nothing will happen.
It create nodes with 
- information about members of parliament
- created documents

Note, that user which run cron job must have rights to create nodes 
of node type:
- sejmometr_mp	
- sejmometr_doc

Before install please ensure that Content Copy is enabled!
Despite that it is required and will be enabled during install
module will fail to create required content types.
